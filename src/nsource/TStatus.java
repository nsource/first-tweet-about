package nsource;

import org.apache.commons.lang3.StringUtils;
import twitter4j.Status;

public class TStatus{
    Status status;
    double JaroWinklerDistance;
    int LevenshteinDistance;
    boolean exactMatch=false;
    boolean cleanMatch=false;
    
    public TStatus(Status status){
        this.status=status;
        this.exactMatch=status.getText().equals(TweetContent.getOriginalText());
        this.cleanMatch=TweetContent.removeOutliers(status.getText(),true).equals(TweetContent.removeOutliers(TweetContent.getOriginalText(),true));
        JaroWinklerDistance=StringUtils.getJaroWinklerDistance(status.getText(), TweetContent.getOriginalText());
        LevenshteinDistance=StringUtils.getLevenshteinDistance(status.getText(), TweetContent.getOriginalText());
    }    
    
}
