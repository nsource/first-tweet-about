package nsource;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.RateLimitStatus;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.ConfigurationBuilder;

public class TwitterClient  implements Comparator<TStatus>{
    
    public Twitter twitter;
    private RequestToken requestToken;
    private final Properties prop;
    private final String propFileName = "res/app.properties";
    
    public TwitterClient() throws Exception{
        prop = new Properties();
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
        if (inputStream != null) {
            prop.load(inputStream);
        } else {
            throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
        }        
    }
    
    public Status loadTweet(Long tweetId) throws Exception{
        
        Map <String,RateLimitStatus>limits=twitter.getRateLimitStatus();
        RateLimitStatus rateLimitStatus = limits.get("/search/tweets");
        if(rateLimitStatus.getRemaining()<8){
            throw new Exception("You won't be able to perform more searches for "+rateLimitStatus.getSecondsUntilReset()+" seconds");
        }
        if(rateLimitStatus.getRemaining()<3){
            return null;
        }
        Status status=twitter.showStatus(tweetId);
        return status;
    }
    
    public ArrayList<TStatus> Search(Main main, String sQuery, long tweetId) throws Exception{
        ArrayList<TStatus> statuses=new ArrayList();
        Map <String,RateLimitStatus>limits=twitter.getRateLimitStatus();
        RateLimitStatus rateLimitStatus = limits.get("/search/tweets");
        if(rateLimitStatus.getRemaining()<8){
            throw new Exception("You won't be able to perform more searches for "+rateLimitStatus.getSecondsUntilReset()+" seconds");
        }
        if(rateLimitStatus.getRemaining()<3){
            return null;
        }
        Boolean hasNext;
        Query query=new Query(sQuery).count(100).resultType(Query.ResultType.recent);
        if(tweetId>0){
            query.maxId(tweetId);
        }

        int soonest=0;
        do{
            soonest+=100;
            main.setStatus("Performing search soonest("+String.valueOf(soonest)+")");
            QueryResult res=twitter.search(query);
            List<Status> tweets=res.getTweets();
            for (Status tweet : tweets) {
                statuses.add(new TStatus(tweet));
            }
            if(res.hasNext()){
                query=res.nextQuery();
                hasNext=true;
            }else{
                hasNext=false;
            }
            if(rateLimitStatus.getRemaining()<1){
                hasNext=false;
            }
        }while(hasNext);
        Collections.sort(statuses, this);
        return statuses;
    }
    
    public URI getAuthURI() throws Exception{
        ConfigurationBuilder cb=new ConfigurationBuilder().setOAuthConsumerKey(prop.getProperty("OAuthConsumerKey"))
                .setOAuthConsumerSecret(prop.getProperty("OAuthConsumerSecret"));
        TwitterFactory tf = new TwitterFactory(cb.build());
        twitter = tf.getInstance();
        requestToken=twitter.getOAuthRequestToken();
        String url=requestToken.getAuthorizationURL();
        return new URI(url);
    }
    
    public void updateAuthInfo(String pin) throws TwitterException, IOException, URISyntaxException{
        OutputStream output;
        AccessToken accessToken = twitter.getOAuthAccessToken(requestToken, pin);
        URL resourceUrl = getClass().getClassLoader().getResource(propFileName);
        File file = new File(resourceUrl.toURI());
        output = new FileOutputStream(file);
        if(output!=null){
            prop.setProperty("token", accessToken.getToken());
            prop.setProperty("secret", accessToken.getTokenSecret());
            prop.store(output, "");
        }
/*        PrintWriter writer = new PrintWriter("access.dat", "UTF-8");
        writer.println(accessToken.getToken());
        writer.println(accessToken.getTokenSecret());
        writer.close();
*/
    }
    
    public Boolean auth() throws Exception{
        if(!AuthInfoExist()){
            return false;
        }
//        List<String> lines=Files.readAllLines(FileSystems.getDefault().getPath("access.dat"), Charset.forName("UTF-8"));
        String token=prop.getProperty("token");//lines.get(0);
        String secret=prop.getProperty("secret");//lines.get(1);
/*        if(output!=null){
            prop.setProperty("token", token);
            prop.setProperty("secret", secret);
            prop.store(output, "");
        }*/
        ConfigurationBuilder cb=new ConfigurationBuilder().setOAuthConsumerKey(prop.getProperty("OAuthConsumerKey"))
                .setOAuthConsumerSecret(prop.getProperty("OAuthConsumerSecret"))
                .setOAuthAccessToken(token).setOAuthAccessTokenSecret(secret);
        TwitterFactory tf = new TwitterFactory(cb.build());
        twitter = tf.getInstance();
        return true;
    }
    
    public Long getTweetIdFromUrl(String URL) throws Exception{
        long tweetid=0L;
        String[] comps=URL.split("/");
        for(int i=0;i<comps.length;i++){
            if(comps[i].equals("status")){
                if(i+1<comps.length){
                    tweetid=Long.valueOf(comps[i+1]);
                }else{
                    throw new Exception("Couldn't extract tweet id from the url");
                }
            }
        }
        return tweetid;
    }
    
    private Boolean AuthInfoExist(){
//        File access=new File("access.dat");
//        return access.exists();
        return prop.getProperty("token")!=null;
    }

    @Override
    public int compare(TStatus o1, TStatus o2) {
        return o1.status.getCreatedAt().compareTo(o2.status.getCreatedAt());
/*        int exact=o1.exactMatch?1000:0;
        int clean=o1.cleanMatch?1000:0;
        int l=o1.LevenshteinDistance;
        int j=(int)o1.JaroWinklerDistance*1000;
        return date+exact+clean+l+j;*/
    }
}
