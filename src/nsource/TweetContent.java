package nsource;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TweetContent {
    static private String sTweetText;
    static private String sTweetCleanText;

    public static String removeOutliers(String text){
        return removeOutliers(text, false);
    }
    public static String removeOutliers(String text, boolean built_in){
        sTweetText=text;
        String local=text;
        local=local.replaceAll("[^\\p{L}\\p{Z}]"," ");
        local=local.replaceAll("عا*جل", "");
        if(!built_in){
            List<String> lines = null;
            try {
                lines=Files.readAllLines(FileSystems.getDefault().getPath("outliers.txt"), Charset.forName("UTF-8"));
            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
            for(int i=0;lines!=null&&i<lines.size();i++){
                local=local.replaceAll(lines.get(i), "");
            }
        }
        sTweetCleanText=local;
        return local;
    }
    
    public static String getOriginalText(){
        return sTweetText;
    }

    public static String getCleanText(){
        return sTweetCleanText;
    }
    
    public static void clean(){
        sTweetCleanText=sTweetText="";
    }
    
}
