#First Tweet About#

##How to Build##
* Rename **/src/res/sample.app.properties** to **/src/res/app.properties**.
* add your Twitter application OAuthConsumerKey, OAuthConsumerSecret to the file replacing **"KEY"**, **"SECRET"**.
* open as Netbeans 8 project and build.

##How to use##
* Copy the tweet URL and paste it to the tweet URL field
* Click **Load**
* The application will load the tweet and remove some common words *The list of words can be found in outliers.txt* then show the resulting text in the search text field.
* You can modify the text if needed then click search.
* The application will perform the search on twitter and display the results with different colors (green means exact match, yellow means close enough).
* There are filter buttons below the results which can be used to filter the displayed tweets.
* To start over you can click **"Clean & Start Over"**.

##Requirements##
* Java 7 or newer.
* Working internet connection.
* An active twitter account that can be used to login to the application and perform the needed searches.